package interfaces;

import java.io.File;

public interface FolderActionListener {
    void openFolder(File file);
}
