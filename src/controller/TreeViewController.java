package controller;

import config.Config;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;

import handler.FileHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class TreeViewController {
    private TreeView<Object> treeView;
    private String path = Config.getRootPath();

    public TreeViewController(TreeView<Object> treeView){
        this.treeView = treeView;
        setTreeView();
        System.out.println(path);
    }

    private List<File> getDirectory(){
        FileHandler.createDir(path);
        return Arrays.asList(FileHandler.listFiles(path));
    }

    private void setTreeView(){
        //Creating tree items
        TreeItem<Object> base = new TreeItem<>(getVBox(new File(path)));
        base.setExpanded(true);

        for ( File file : getDirectory()){
            base.getChildren().add(setTreeView(file));
        }
        //Creating a TreeView item
        treeView.setRoot(base);
    }

    private TreeItem<Object> setTreeView(File file){

        TreeItem<Object> treeView = new TreeItem<>(getVBox(file));

        return treeView;
    }

    private HBox getVBox(File file){
        HBox vBox = new HBox();
        Label label = new Label(file.getName());
        label.getStyleClass().add("folder-label");
        label.setFont(new Font(16));
        FontAwesomeIconView icon = new FontAwesomeIconView(FontAwesomeIcon.FOLDER);
        icon.setGlyphSize(20);
        icon.getStyleClass().add("folder-icon");
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.getChildren().add(icon);
        vBox.getChildren().add(label);

        return vBox;
    }

}
