package controller;

import handler.FileHandler;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import org.reactfx.EventSource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExplorerHandler {

    private EventSource<Node> showDirContentEvent;
    private TilePane tilePane;
    private ScrollPane scrollPane;

    public ExplorerHandler(TilePane tilePane){
        this.tilePane = tilePane;
        scrollPane = (ScrollPane) this.tilePane.getParent().getParent();
        showDirContentEvent = new EventSource<>();
        setEvents();
    }

    public void showCurrentDirContents(String path){
        List<File> fileList = new ArrayList<>(Arrays.asList(FileHandler.listFiles(path)));

        for (File file: fileList){
            AnchorPane node = (AnchorPane)infrateLayout();
            FolderController folderController = new FolderController(node, file);
            showDirContentEvent.push(folderController.getNode());
        }
    }

    private void setEvents(){
        showDirContentEvent.subscribe(node -> {
            if (node != null) {
                Platform.runLater(()->{
                    tilePane.getChildren().add(node);
                });
            }
        });
    }

    private Node infrateLayout(){
        Node node = null;
        try {
            node = FXMLLoader.load(getClass().getResource("../view/folder-layout.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return node;
    }

    private void setTilePaneScrollListener(){
        tilePane.heightProperty().addListener((ChangeListener) (observable, oldvalue, newValue) -> {
            Platform.runLater(()->{
                scrollPane.setHvalue((Double) newValue);
            });
        });
    }


}
