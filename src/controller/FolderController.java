package controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import helper.HelperMethods;
import interfaces.FolderActionListener;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.io.File;

import static helper.HelperMethods.getFileExtension;

public class FolderController {

    private FolderActionListener folderActionListener;
    private AnchorPane anchorPane;
    private VBox folder_container;
    private File folder;
    private Label lb_folder_name;
    private ImageView file_image;

    public FolderController(AnchorPane node, File file) {
        folder = file;
        anchorPane = node;
        initComponents();
    }

    private void initComponents() {
        folder_container = (VBox) anchorPane.getChildren().get(0);
        file_image = (ImageView) folder_container.getChildren().get(0);
//        folder_container.getChildren().add(0, getIcon());
        lb_folder_name = (Label) folder_container.getChildren().get(1);
        lb_folder_name.setText(folder.getName());

        folderActionListener = MainController.folderActionListener;

        folder_container.setOnMouseClicked(event -> {
            if (folder.isDirectory()){
                folderActionListener.openFolder(folder);
            }
        });

        setImageBg();
    }

//    private Node getIcon() {
//        FontAwesomeIcon icon;
//
//        if (folder.isDirectory()) {
//            icon = FontAwesomeIcon.FOLDER;
//        } else {
////            icon = HelperMethods.getIcon(getFileExtension(folder));
//        }

//        else if (folder.getName().endsWith(".pdf")) {
//            icon = FontAwesomeIcon.FILE_PDF_ALT;
//        }else {
//            String ext = getFileExtension(folder);
//
//            System.out.println(ext);
////            switch (ext){
////                case
////            }
////            System.out.println(folder.);
//            icon = FontAwesomeIcon.FILE;
//        }

//        FontAwesomeIconView iconView = new FontAwesomeIconView(icon);
//        iconView.setGlyphSize(50);
//        iconView.getStyleClass().add("folder-icon");
//        return iconView;
//    }

    private void setImageBg(){
        file_image.setImage(HelperMethods.getIcon(folder));

//        System.out.println("Image: " + img.getHeight() + " X " + img.getWidth());
//        BackgroundImage bgImage = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
//                BackgroundSize.DEFAULT);
//
//        Background bg = new Background(bgImage);
//        pane_image.setBackground(bg);
    }

    public Node getNode(){
        return anchorPane;
    }



}
