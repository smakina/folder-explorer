package controller;

import config.Config;
import handler.FileHandler;
import handler.NavigationBreadCrumbs;
import interfaces.FolderActionListener;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    public static FolderActionListener folderActionListener;

    @FXML
    TreeView<Object> treeView;

    @FXML
    AnchorPane navbContainer;

    @FXML
    TilePane explorer_folder_box;

    private ExplorerHandler explorerHandler;
    private NavigationBreadCrumbs navigationBreadCrumbs;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initFolder();
        new TreeViewController(treeView);
        explorerHandler = new ExplorerHandler(explorer_folder_box);
        navigationBreadCrumbs = new NavigationBreadCrumbs(navbContainer);
        showFolder(Config.getRootPath());
    }

    public void initFolder(){
        folderActionListener = new FolderActionListener() {
            @Override
            public void openFolder(File file) {
                showFolder(file.getAbsolutePath());
                navigationBreadCrumbs.setPath(file.getAbsolutePath());
            }
        };
    }

    private void showFolder(String path){
        explorer_folder_box.getChildren().clear();
        Thread thread = new Thread(() -> {
            explorerHandler.showCurrentDirContents(path);
        });
        thread.start();
    }

}
