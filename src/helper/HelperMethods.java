package helper;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.image.Image;

import java.io.File;

public class HelperMethods {

    public static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    public static Image getIcon(File file) {
        String ext = getFileExtension(file);
        String path = "images/icons/";

        switch (ext) {
            case ".png":
            case ".jpg":
            case ".jpeg":
                path = path+"image_60px.png";
                break;
            case ".pdf":
                path = path+"pdf_60px.png";
                break;
            case ".rar":
                path = path+"winrar_60px.png";
                break;
            case ".db":
                path = path+"database_60px.png";
                break;
            default:
                if (file.isDirectory())
                    path = path+"folder_60px.png";
                else
                    path = path+"document_60px.png";
                break;
        }

        return new Image(path);
    }
}
