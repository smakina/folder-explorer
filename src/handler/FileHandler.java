package handler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHandler {

    public static boolean createDir(String path) {
        File file = new File(path);

        if (!file.exists()) {
            return file.mkdirs();
        }

        return false;
    }

    public static File[] listFiles(String path) {
        File file = new File(path);
        return file.listFiles();
    }

//    public void listFiles() throws IOException {
//        Path dir = Paths.get("/path/to/dir");
//        Files.walk(dir).forEach(path -> showFile(path.toFile()));
//    }
//
//    public static void showFile(File file) {
//        if (file.isDirectory()) {
//            System.out.println("Directory: " + file.getAbsolutePath());
//        } else {
//            System.out.println("File: " + file.getAbsolutePath());
//        }
//    }
//
//    public void showFiles() {
//        File dir = new File("/path/to/dir");
//        showFiles(dir.listFiles());
//    }
//
//    public static void showFiles(File[] files) {
//        for (File file : files) {
//            if (file.isDirectory()) {
//                System.out.println("Directory: " + file.getAbsolutePath());
//                showFiles(file.listFiles()); // Calls same method again.
//            } else {
//                System.out.println("File: " + file.getAbsolutePath());
//            }
//        }
//    }
//

}
