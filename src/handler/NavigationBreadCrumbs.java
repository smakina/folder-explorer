package handler;

import controller.MainController;
import interfaces.FolderActionListener;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.BreadCrumbBar;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static config.Config.getRootPath;

public class NavigationBreadCrumbs {

    private FolderActionListener folderActionListener;
    private String path;
    private AnchorPane container;

    public NavigationBreadCrumbs(AnchorPane container){
        this.path = getRootPath();
        this.container = container;
        folderActionListener = MainController.folderActionListener;
        setBreadCrumbs();
    }

    public void setPath(String path) {
        System.out.println(path);
        this.path = path;
        setBreadCrumbs();
    }

    private void setBreadCrumbs(){
        BreadCrumbBar<String> sampleBreadCrumbBar = new BreadCrumbBar<>();

        TreeItem<String> model = BreadCrumbBar.buildTreeModel(getPathList());
        sampleBreadCrumbBar.setSelectedCrumb(model);

        sampleBreadCrumbBar.setOnCrumbAction(bae -> {
            String p = path.split(bae.getSelectedCrumb().getValue())[0];
            p = String.format("%s%s", p, bae.getSelectedCrumb().getValue() );
            System.out.println(p);
            folderActionListener.openFolder(new File(p));
//                selectedCrumbLbl.setText("You just clicked on '" + bae.getSelectedCrumb() + "'!");
        });

        container.getChildren().clear();
        AnchorPane.setLeftAnchor(sampleBreadCrumbBar, 0.5);
        AnchorPane.setTopAnchor(sampleBreadCrumbBar, 0.5);
        AnchorPane.setRightAnchor(sampleBreadCrumbBar, 100.0);
        AnchorPane.setBottomAnchor(sampleBreadCrumbBar, 0.5);
        container.getChildren().add(sampleBreadCrumbBar);
    }

    private String[] getPathList(){
        String[] p = path.split("\\\\");

        int index = 5;
        int x = 0;

        String[] path= new String[p.length - index];

        for (String s: p){
            System.out.println(s);
            if ( x >= index){
                path[x - index] = s;
            }
            x++;
        }

        System.out.println(path.length);

        return path;
    }

}
